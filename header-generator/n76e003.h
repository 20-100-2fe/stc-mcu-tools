/*
 * Copyright (c) 2022 Vincent DEFERT. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions 
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright 
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright 
 * notice, this list of conditions and the following disclaimer in the 
 * documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file n76e003.h
 * 
 * Register, bit and macro definitions for the N76E003
 */

#ifndef _N76E003_H
#define _N76E003_H

#include <compiler.h>

// SFR P0: Port 0
SFR(P0, 0x80);
SBIT(P0_0, 0x80, 0);
SBIT(P0_1, 0x80, 1);
SBIT(P0_2, 0x80, 2);
SBIT(P0_3, 0x80, 3);
SBIT(P0_4, 0x80, 4);
SBIT(P0_5, 0x80, 5);
SBIT(P0_6, 0x80, 6);
SBIT(P0_7, 0x80, 7);

// SFR SP: Stack Pointer
SFR(SP, 0x81);

// SFR DPL: Data Pointer (DPTR) low
SFR(DPL, 0x82);

// SFR DPH: Data Pointer (DPTR) high
SFR(DPH, 0x83);

// SFR RCTRIM0: 
SFR(RCTRIM0, 0x84);

// SFR RCTRIM1: 
SFR(RCTRIM1, 0x85);

// SFR RWK: 
SFR(RWK, 0x86);

// SFR PCON: Power control
SFR(PCON, 0x87);
#define IDL 0x1
#define PD 0x2
#define GF0 0x4
#define GF1 0x8
#define POF 0x10
#define LVDF 0x20
#define SMOD0 0x40
#define SMOD 0x80

// SFR TCON: Timer control
SFR(TCON, 0x88);
SBIT(IT0, 0x88, 0);
SBIT(IE0, 0x88, 1);
SBIT(IT1, 0x88, 2);
SBIT(IE1, 0x88, 3);
SBIT(TR0, 0x88, 4);
SBIT(TF0, 0x88, 5);
SBIT(TR1, 0x88, 6);
SBIT(TF1, 0x88, 7);

// SFR TMOD: Timer mode
SFR(TMOD, 0x89);
#define T0_M0 0x1
#define T0_M1 0x2
#define T0_C_T 0x4
#define T0_GATE 0x8
#define T1_M0 0x10
#define T1_M1 0x20
#define T1_C_T 0x40
#define T1_GATE 0x80

// SFR TL0: Timer 0 low
SFR(TL0, 0x8A);

// SFR TL1: Timer 1 low
SFR(TL1, 0x8B);

// SFR TH0: Timer 0 high
SFR(TH0, 0x8C);

// SFR TH1: Timer 1 high
SFR(TH1, 0x8D);

// SFR CKCON: 
SFR(CKCON, 0x8E);

// SFR WKCON: 
SFR(WKCON, 0x8F);

// SFR P1: Port 1
SFR(P1, 0x90);
SBIT(P1_0, 0x90, 0);
SBIT(P1_1, 0x90, 1);
SBIT(P1_2, 0x90, 2);
SBIT(P1_3, 0x90, 3);
SBIT(P1_4, 0x90, 4);
SBIT(P1_5, 0x90, 5);
SBIT(P1_6, 0x90, 6);
SBIT(P1_7, 0x90, 7);

// SFR SFRS: Port 1 Configuration 1
SFR(SFRS, 0x91);

// SFR CAPCON0: 
SFR(CAPCON0, 0x92);

// SFR CAPCON1: 
SFR(CAPCON1, 0x93);

// SFR CAPCON2: 
SFR(CAPCON2, 0x94);

// SFR CKDIV: 
SFR(CKDIV, 0x95);

// SFR CKSWT: 
SFR(CKSWT, 0x96);

// SFR CKEN: 
SFR(CKEN, 0x97);

// SFR SCON: Serial control
SFR(SCON, 0x98);
SBIT(RI, 0x98, 0);
SBIT(TI, 0x98, 1);
SBIT(RB8, 0x98, 2);
SBIT(TB8, 0x98, 3);
SBIT(REN, 0x98, 4);
SBIT(SM2, 0x98, 5);
SBIT(SM1, 0x98, 6);
SBIT(SM0_FE, 0x98, 7);

// SFR SBUF: Serial buffer
SFR(SBUF, 0x99);

// SFR SBUF_1: 
SFR(SBUF_1, 0x9A);

// SFR EIE: 
SFR(EIE, 0x9B);

// SFR EIE1: 
SFR(EIE1, 0x9C);

// SFR CHPCON: 
SFR(CHPCON, 0x9F);

// SFR P2: Port 2
SFR(P2, 0xA0);
SBIT(P2_0, 0xA0, 0);
SBIT(P2_1, 0xA0, 1);
SBIT(P2_2, 0xA0, 2);
SBIT(P2_3, 0xA0, 3);
SBIT(P2_4, 0xA0, 4);
SBIT(P2_5, 0xA0, 5);
SBIT(P2_6, 0xA0, 6);
SBIT(P2_7, 0xA0, 7);

// SFR AUXR1: 
SFR(AUXR1, 0xA2);

// SFR BODCON0: 
SFR(BODCON0, 0xA3);

// SFR IAPTRG: 
SFR(IAPTRG, 0xA4);

// SFR IAPUEN: 
SFR(IAPUEN, 0xA5);

// SFR IAPAL: 
SFR(IAPAL, 0xA6);

// SFR IAPAH: 
SFR(IAPAH, 0xA7);

// SFR IE: Interrupt enable
SFR(IE, 0xA8);
SBIT(EX0, 0xA8, 0);
SBIT(ET0, 0xA8, 1);
SBIT(EX1, 0xA8, 2);
SBIT(ET1, 0xA8, 3);
SBIT(ES, 0xA8, 4);
SBIT(EADC, 0xA8, 5);
SBIT(ELVD, 0xA8, 6);
SBIT(EA, 0xA8, 7);

// SFR SADDR: Slave address
SFR(SADDR, 0xA9);

// SFR WDCON: 
SFR(WDCON, 0xAA);

// SFR BODCON1: 
SFR(BODCON1, 0xAB);

// SFR P3M1: 
SFR(P3M1, 0xAC);

// SFR P3S: 
SFR(P3S, 0xAC);

// SFR P3M2: 
SFR(P3M2, 0xAD);

// SFR P3SR: 
SFR(P3SR, 0xAD);

// SFR IAPFD: 
SFR(IAPFD, 0xAE);

// SFR IAPCN: 
SFR(IAPCN, 0xAF);

// SFR P3: Port 3
SFR(P3, 0xB0);
SBIT(P3_0, 0xB0, 0);
SBIT(P3_1, 0xB0, 1);
SBIT(P3_2, 0xB0, 2);
SBIT(P3_3, 0xB0, 3);
SBIT(P3_4, 0xB0, 4);
SBIT(P3_5, 0xB0, 5);
SBIT(P3_6, 0xB0, 6);
SBIT(P3_7, 0xB0, 7);

// SFR P0M1: 
SFR(P0M1, 0xB1);

// SFR P0S: 
SFR(P0S, 0xB1);

// SFR P0M2: 
SFR(P0M2, 0xB2);

// SFR P0SR: 
SFR(P0SR, 0xB2);

// SFR P1M1: 
SFR(P1M1, 0xB3);

// SFR P1S: 
SFR(P1S, 0xB3);

// SFR P1M2: 
SFR(P1M2, 0xB4);

// SFR P1SR: 
SFR(P1SR, 0xB4);

// SFR P2S: 
SFR(P2S, 0xB5);

// SFR IPH: 
SFR(IPH, 0xB7);

// SFR PWMINTC: 
SFR(PWMINTC, 0xB7);

// SFR IP: Interrupt priority register low
SFR(IP, 0xB8);
SBIT(PX0, 0xB8, 0);
SBIT(PT0, 0xB8, 1);
SBIT(PX1, 0xB8, 2);
SBIT(PT1, 0xB8, 3);
SBIT(PS, 0xB8, 4);
SBIT(PADC, 0xB8, 5);
SBIT(PLVD, 0xB8, 6);
SBIT(PPCA, 0xB8, 7);

// SFR SADEN: Slave address mask
SFR(SADEN, 0xB9);

// SFR SADEN_1: Slave address mask
SFR(SADEN_1, 0xBA);

// SFR SADDR_1: 
SFR(SADDR_1, 0xBB);

// SFR I2DAT: 
SFR(I2DAT, 0xBC);

// SFR I2STAT: 
SFR(I2STAT, 0xBD);

// SFR I2CLK: 
SFR(I2CLK, 0xBE);

// SFR I2TOC: 
SFR(I2TOC, 0xBF);

// SFR I2CON: 
SFR(I2CON, 0xC0);

// SFR I2ADR: 
SFR(I2ADR, 0xC1);

// SFR ADCRL: 
SFR(ADCRL, 0xC2);

// SFR ADCRH: 
SFR(ADCRH, 0xC3);

// SFR T3CON: 
SFR(T3CON, 0xC4);

// SFR PWM4H: 
SFR(PWM4H, 0xC4);

// SFR RL3: 
SFR(RL3, 0xC5);

// SFR PWM5H: 
SFR(PWM5H, 0xC5);

// SFR RH3: 
SFR(RH3, 0xC6);

// SFR PIOCON1: 
SFR(PIOCON1, 0xC6);

// SFR TA: 
SFR(TA, 0xC7);

// SFR T2CON: 
SFR(T2CON, 0xC8);

// SFR T2MOD: 
SFR(T2MOD, 0xC9);

// SFR RCMP2L: 
SFR(RCMP2L, 0xCA);

// SFR RCMP2H: 
SFR(RCMP2H, 0xCB);

// SFR TL2: 
SFR(TL2, 0xCC);

// SFR PWM4L: 
SFR(PWM4L, 0xCC);

// SFR TH2: 
SFR(TH2, 0xCD);

// SFR PWM5L: 
SFR(PWM5L, 0xCD);

// SFR ADCMPL: 
SFR(ADCMPL, 0xCE);

// SFR ADCMPH: 
SFR(ADCMPH, 0xCF);

// SFR PSW: Program status word
SFR(PSW, 0xD0);
SBIT(P, 0xD0, 0);
SBIT(F1, 0xD0, 1);
SBIT(OV, 0xD0, 2);
SBIT(RS0, 0xD0, 3);
SBIT(RS1, 0xD0, 4);
SBIT(F0, 0xD0, 5);
SBIT(AC, 0xD0, 6);
SBIT(CY, 0xD0, 7);

// SFR PWMPH: 
SFR(PWMPH, 0xD1);

// SFR PWM0H: 
SFR(PWM0H, 0xD2);

// SFR PWM1H: 
SFR(PWM1H, 0xD3);

// SFR PWM2H: 
SFR(PWM2H, 0xD4);

// SFR PWM3H: 
SFR(PWM3H, 0xD5);

// SFR PNP: 
SFR(PNP, 0xD6);

// SFR FBD: 
SFR(FBD, 0xD7);

// SFR PWMCON0: 
SFR(PWMCON0, 0xD8);

// SFR PWMPL: 
SFR(PWMPL, 0xD9);

// SFR PWM0L: 
SFR(PWM0L, 0xDA);

// SFR PWM1L: 
SFR(PWM1L, 0xDB);

// SFR PWM2L: 
SFR(PWM2L, 0xDC);

// SFR PWM3L: 
SFR(PWM3L, 0xDD);

// SFR PIOCON0: 
SFR(PIOCON0, 0xDE);

// SFR PWMCON1: 
SFR(PWMCON1, 0xDF);

// SFR ACC: Accumulator
SFR(ACC, 0xE0);

// SFR ADCCON1: 
SFR(ADCCON1, 0xE1);

// SFR ADCCON2: 
SFR(ADCCON2, 0xE2);

// SFR ADCDLY: 
SFR(ADCDLY, 0xE3);

// SFR C0L: 
SFR(C0L, 0xE4);

// SFR C0H: 
SFR(C0H, 0xE5);

// SFR C1L: 
SFR(C1L, 0xE6);

// SFR C1H: 
SFR(C1H, 0xE7);

// SFR ADCCON0: 
SFR(ADCCON0, 0xE8);

// SFR PICON: 
SFR(PICON, 0xE9);

// SFR PINEN: 
SFR(PINEN, 0xEA);

// SFR PIPEN: 
SFR(PIPEN, 0xEB);

// SFR PIF: 
SFR(PIF, 0xEC);

// SFR C2L: 
SFR(C2L, 0xED);

// SFR C2H: 
SFR(C2H, 0xEE);

// SFR EIP: 
SFR(EIP, 0xEF);

// SFR B: B register
SFR(B, 0xF0);

// SFR CAPCON3: 
SFR(CAPCON3, 0xF1);

// SFR CAPCON4: 
SFR(CAPCON4, 0xF2);

// SFR SPCR: 
SFR(SPCR, 0xF3);

// SFR SPCR2: 
SFR(SPCR2, 0xF3);

// SFR SPSR: 
SFR(SPSR, 0xF4);

// SFR SPDR: 
SFR(SPDR, 0xF5);

// SFR AINDIDS: 
SFR(AINDIDS, 0xF6);

// SFR EIPH: 
SFR(EIPH, 0xF7);

// SFR SCON_1: 
SFR(SCON_1, 0xF8);

// SFR PDTEN: 
SFR(PDTEN, 0xF9);

// SFR PDTCNT: 
SFR(PDTCNT, 0xFA);

// SFR PMEN: 
SFR(PMEN, 0xFB);

// SFR PMD: 
SFR(PMD, 0xFC);

// SFR PORDIS: 
SFR(PORDIS, 0xFD);

// SFR EIP1: 
SFR(EIP1, 0xFE);

// SFR EIPH1: 
SFR(EIPH1, 0xFF);

// Default value for macro F_CPU
#ifndef F_CPU
#define F_CPU 16000000UL
#endif

// Default value for macro T_CPU
#ifndef T_CPU
#define T_CPU 1
#endif

inline void selectSFRPage0() { asm { MOV TA
inline void selectSFRPage1() { asm { MOV TA

#endif // _N76E003_H
